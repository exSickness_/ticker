#include "dataProc.h"


int readFile(int argc, char* argv[],market* nameMarket)
{
	FILE* fp = NULL;
	if(argc == 2)
	{
		fp = fopen(argv[1], "r+");
		if(!fp) {
			return(-1);
		}
	}
	else {
		return(-1);
	}
	size_t tempPriceSz = 12,tempSymbolSz = 7, tempNameSz = 66;
	char* tempSymbol = malloc(tempSymbolSz);
	char* tempPrice = malloc(tempPriceSz);
	size_t sztPrice;
	char* tempName = malloc(tempNameSz);
	size_t sz = 82;
	char *buf = malloc(sz * sizeof(buf));
	char* checkForDes;
	char* comment;
	int checkForSign = 0;
	while(getline(&buf,&sz,fp) != -1)
	{
		if(( comment = strchr(buf,'#')) != NULL) // found pound
		{
			comment[0] = '\n';
			comment[1] = '\0';
		}
		if(validateLine(buf,checkForSign) == -1) {
			continue;
		}
		strncpy( tempSymbol, strtok(buf," "), tempSymbolSz);
		strncpy( tempPrice, strtok(NULL," "), tempPriceSz);
		if( (checkForDes = strtok(NULL,"\0")) == NULL)
		{
			tempPrice[strlen(tempPrice) -1] = '\n';
			tempPrice[strlen(tempPrice) ] = '\0';
			tempName[0] = '\n';
			tempName[1] = '\0';
		}
		else {
			strncpy( tempName, checkForDes,tempNameSz );
		}
		sztPrice = extractCents(tempPrice);

		struct company* tempComp = stock_create(tempSymbol,tempName,sztPrice);
		tree_insert(nameMarket->root,tempComp,nameMarket->cmp);
	}
	free(buf);
	free(tempName);
	free(tempPrice);
	free(tempSymbol);
	fclose(fp);
	return(0);
}


size_t extractCents(char* str)
{
	char* endptr;
	char* endOfStr = strchr(str,'.');
	endOfStr[3] = '\0';
	size_t total = strtol(strtok(str,"."),&endptr,10);
	total *= 100;
	total += strtol(strtok(NULL,""),&endptr,10);
	if(total >= 100000000) {
		total = 100000000;
	}
	return(total);
}


int recieveData(market* nameMarket)
{
	char* tempSymbol = malloc(7);
	char* tempPrice = malloc(12);
	size_t sztPrice;
	char* tempName = malloc(65);
	size_t sz = 82;
	char *buf = malloc(sz * sizeof(buf));
	char* checkForDes;
	int add = 0;
	char* comment;
	int checkForSign = 1;
	while(getline(&buf,&sz,stdin) != -1)
	{
		if(( comment = strchr(buf,'#')) != NULL) // found pound
		{
			comment[0] = '\n';
			comment[1] = '\0';
		}
		if(validateLine(buf,checkForSign) == -1) {
			continue;
		}
		strcpy( tempSymbol, strtok(buf," ") );
		strcpy( tempPrice, strtok(NULL," ") );
		if( (checkForDes = strtok(NULL,"\0")) == NULL)
		{
			tempPrice[strlen(tempPrice) -1] = '\n';
			tempPrice[strlen(tempPrice) ] = '\0';
			tempName[0] = '\n';
			tempName[1] = '\0';
		}
		else {
			strcpy( tempName, checkForDes );
		}
		add = 0;
		if(tempPrice[0] == '+')
		{
			add = 1;
		}
		sztPrice = extractCents(&tempPrice[1]);
		if(sztPrice <= 0) {
			continue;
		}

		struct company* tempComp = stock_create(tempSymbol,tempName,sztPrice);

		if(!tree_add(nameMarket->root,tempComp,add,nameMarket->cmp)) {// Could not add tempComp to market
			tree_insert(nameMarket->root,tempComp,nameMarket->cmp);
		}
	}
	free(buf);
	free(tempName);
	free(tempPrice);
	free(tempSymbol);
	return(0);
}


int validateLine(char* buf, int checkForSign)
{
	if(!buf) {
		return(-1);
	}
	int i = 0;
	int symbolCount = 0;
	while(buf[i] && buf[i] != ' ') // loop to validate symbol
	{
		if(buf[i] == '#') {
			return(-1);
		}
		if(symbolCount > 5) {
			return(-1);
		}
		if( !isascii(buf[i]) ) {
			return(-1);
		}
		symbolCount++;
		i++;
	}
	if(!buf[i]) {
		return(-1);
	}
	i++;
	if(checkForSign == 1)
	{
		if(buf[i] == '+') {
			i++;
		}
		else if(buf[i] == '-') {
			i++;
		}
		else {
			return(-1);
		}
	}
	int dollarCount = 0;
	while(buf[i] && buf[i] != '.') // loop to validate dollars
	{
		if(buf[i] == '#') {
			return(-1);
		}
		if(dollarCount >= 7) {
			return(-1);
		}
		if( !isdigit(buf[i]) ) {
			return(-1);
		}
		dollarCount++;
		i++;
	}
	if(!buf[i]) {
		return(-1);
	}
	i++;
	int decimalCount = 0;
	while(buf[i] && buf[i] != ' ') // loop to validate cents
	{
		if(buf[i] == '#') {
			return(-1);
		}
		if(buf[i] == '\n' && decimalCount < 3) {
			return(1); // no name
		}
		if(decimalCount >= 3) {
			return(-1);
		}
		if( !isdigit(buf[i]) ) {
			return(-1);
		}
		decimalCount++;
		i++;
	}
	if(!buf[i]) {
		return(-1); // no name
	}
	i++;
	int nameCount = 0;
	while(buf[i] && buf[i] != '\n') // loop to validate name
	{
		if(buf[i] == '#') {
			return(-1);
		}
		if(nameCount > 67) {
			return(-1);
		}
		if( !isascii(buf[i]) ) {
			return(-1);
		}
		nameCount++;
		i++;
	}
	if(!buf[i]) {
		return(-1);
	}
	
	return(0);
}

