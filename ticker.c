#include "ticker.h"

#include <string.h>
#include <stdio.h>
#include <ctype.h>


int main(int argc, char* argv[])
{
	market* priceMarket = malloc(sizeof(*priceMarket));
	struct company* priceRootStock = stock_create("ZZZZZ","ZZZZZ\n",20000);
	priceMarket->root = tree_create(priceRootStock);
	priceMarket->cmp = cmp;

	market* nameMarket = malloc(sizeof(*nameMarket));
	struct company* nameRootStock = stock_create("ZZZZZ","ZZZZZ\n",20000);
	nameMarket->root = tree_create(nameRootStock);
	nameMarket->cmp = cmpName;

	if(readFile(argc,argv,nameMarket) == -1) { // populate market with argv[1] file
		return(-1);
	}
	
	recieveData(nameMarket); // populate market with stdin input

	populatePriceTree(priceMarket->root, nameMarket->root, priceMarket->cmp); // populate market sorted by price

	print_tree(priceMarket->root); // print the price sorted market

	tree_disassemble(nameMarket->root);

	free(nameRootStock->name);
	free(nameRootStock);
	free(nameMarket);

	tree_destroy(priceMarket->root);
	free(priceMarket);

	return(0);
}


int cmpName(const struct company* x, const struct company* y)
{
	int retVal = strcmp(x->symbol,y->symbol);
	return(retVal);
}


int cmp(const struct company* x, const struct company* y)
{
	if(x->cents < y->cents)
	{
		return(-1);
	}
	else if(x->cents > y->cents)
	{
		return(1);
	}
	else
	{
		return(0);
	}
}


struct company* stock_create(char* symbol, char* name, size_t price)
{
	struct company* new_stock = malloc( sizeof(*new_stock) );
	if(!new_stock) {
		return(NULL);
	}

	new_stock->name = strdup(name);
	if(!new_stock->name)
	{
		free(new_stock);
		return(NULL);
	}

	strncpy(new_stock->symbol, symbol, sizeof( new_stock->symbol ) -1);
	new_stock->symbol[ sizeof(new_stock->symbol) -1] = '\0';

	new_stock->cents = price;

	return(new_stock);
}
