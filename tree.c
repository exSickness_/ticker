#include "tree.h"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct tree* tree_create(struct company* data)
{
	struct tree* t = malloc(sizeof(*t));
	if(t) {
		t->data = data;
		t->left = NULL;
		t->right = NULL;
	}

	return(t);
}


void tree_destroy(struct tree* t)
{
	if(!t) {
		return;
	}

	tree_destroy(t->left); // yay for recursion!
	tree_destroy(t->right);

	free(t->data->name);
	free(t->data);
	free(t);
}


void tree_disassemble(struct tree *t)
{
	if (!t) {
		return;
	}
	tree_disassemble(t->left);
	tree_disassemble(t->right);

	free(t);
}


void print_tree(struct tree* t)
{
	if(!t) {
		return;
	}

	print_tree(t->left); // yay for recursion!
	if(strcmp(t->data->symbol,"ZZZZZ") != 0) // skip the dummy stock
	{
		printf("%s %zd.%02zd %s",t->data->symbol,t->data->cents / 100,t->data->cents % 100,t->data->name);
		// printf("%6s %6zd.%02zd %s",t->data->symbol,t->data->cents / 100,t->data->cents % 100,t->data->name);
	}
	print_tree(t->right);
}


bool tree_insert(struct tree* t, struct company* comp, int (*cmp)(const struct company* x, const struct company* y))
{
	if(strcmp(comp->symbol, "ZZZZZ") == 0) {
		return(true);
	}
	if(strcmp(comp->symbol, t->data->symbol) == 0)
	{
 		// same name found. add price to current price
 		t->data->cents += comp->cents;
 		free(comp->name);
 		free(comp);
 		return(0);
	}
	if(cmp(comp,t->data) < 1) // check for left child
	{
		if(t->left) { // go down the left child
			return( tree_insert(t->left, comp, cmp) );
		}
		else // no left child found. insert tree here
		{
			t->left = tree_create(comp);
			return(t->left);
		}
	}
	else // name is greater than current tree
	{
		if(t->right) { // go down the right child
			return( tree_insert(t->right, comp, cmp) );
		}
		else // no right child found. insert tree here
		{
			t->right = tree_create(comp);
			return(t->right);
		}
	}
}


bool tree_add(struct tree* t, struct company* comp, int add, int (*cmp)(const struct company* x, const struct company* y))
{
	if(!t) {
		return(false);
	}
	if(strcmp(comp->symbol, t->data->symbol) == 0)
	{
 		// same name found. add price to current price
 		if(add == 1)
		{
			if((t->data->cents + comp->cents) >= 100000000) {
				t->data->cents = 100000000;
			}
			else {
				t->data->cents += comp->cents;
			}
		}
		else
		{
			if( t->data->cents <= comp->cents)
			{
				fprintf(stderr,"Adjustment will take price below $0.01.  Skipping adjustment.\n");
			}
			else {
				t->data->cents -= comp->cents;
			}
		}
 		free(comp->name);
		free(comp);
 		return(true);
	}
	if(cmp(comp,t->data) < 1) // check for left child
	{
		return( tree_add(t->left, comp, add,cmp) );
	}
	else // name is greater than current tree
	{
		return( tree_add(t->right, comp, add,cmp) );
	}
}


void populatePriceTree(struct tree* priceMkt, struct tree* nameMkt,int (*cmp)(const struct company* x, const struct company* y))
{
	if(!nameMkt) {
		return;
	}

	populatePriceTree(priceMkt,nameMkt->left,cmp); // yay for recursion!
	tree_insert(priceMkt,nameMkt->data,cmp);
	populatePriceTree(priceMkt,nameMkt->right,cmp);
}
