#ifndef TREE_H
	#define TREE_H

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

struct company{
	char symbol[6];
	size_t cents;
	char* name;
};

struct tree{
	struct company* data;
	struct tree* left;
	struct tree* right;
};

typedef struct{
	struct tree* root;
	int (*cmp)(const struct company* x, const struct company* y);
} market;


bool tree_insert(struct tree* t, struct company* comp, int (*cmp)(const struct company* x, const struct company* y));
bool tree_add(struct tree* t, struct company* comp, int add, int (*cmp)(const struct company* x, const struct company* y));
void populatePriceTree(struct tree* t, struct tree* x,int (*cmp)(const struct company* x, const struct company* y));
void print_tree(struct tree* t);
struct tree* tree_create(struct company* data);
void tree_destroy(struct tree* t);
void tree_disassemble(struct tree *t);


#endif