#ifndef TICKER_H
	#define TICKER_H

#include "tree.h"
#include "dataProc.h"

struct company* stock_create(char* symbol, char* name, size_t price);
int cmpName(const struct company* x, const struct company* y);
int cmp(const struct company* x, const struct company* y);


#endif