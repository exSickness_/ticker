#ifndef DATAPROC_H
	#define DATAPROC_H

#include "tree.h"
#include "ticker.h"

int readFile(int argc, char* argv[],market* nameMarket);
size_t extractCents(char* str);
int recieveData(market* nameMarket);
int validateLine(char* buf, int checkForSign);

#endif