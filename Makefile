CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic -fstack-usage -D _XOPEN_SOURCE=800


ticker: ticker.o tree.o dataProc.o


.PHONY: clean debug profile

clean:
	rm ticker *.o *.su

debug: CFLAGS+=-g
debug: ticker

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: ticker